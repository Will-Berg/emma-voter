import './App.css';
import {React} from 'react'
import Candidate from './Candidate'

const Year = ({members, year, activeYear, setActiveYear, chosen, setChosen, getResult}) => {
  return (
    <div className="year" style={(year === activeYear) ? 
      {backgroundColor: "#EEDD44"}
      : {backgroundColor: "#BBAA88"}
    }>
      <h1>{year}</h1>
      {members.map((name) => (
        <Candidate name={name} year={year} activeYear={activeYear} setActiveYear={setActiveYear} chosen={chosen} setChosen={setChosen}/>
      ))}
      <button
        className={"submit" +
          (year !== activeYear ? " inaSubmit" : "")
        }
        onClick={() => {
          setActiveYear(year);
          getResult(chosen, year);
        }}
      >
        SUBMIT
      </button>
    </div>
  )
}

export default Year