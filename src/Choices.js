const choices = {
  "2025" : {
    "President" : [
      'Emma Berg',
      'Ava Rice',
      'Abigail Dominica',
      'Levin McCoy',
      'Madeline Dean',
      'Natalii Rizo'
    ],
    "Vice President" : [
      'Emma Berg',
      'Ava Rice',
      'Abigail Dominica',
      'Levin McCoy',
      'Madeline Dean',
      'Natalii Rizo'
    ],
    "Secretary" : [
      'Emma Berg',
      'Ava Rice',
      'Abigail Dominica',
      'Levin McCoy',
      'Madeline Dean',
      'Natalii Rizo'
    ],
    "Treasurer" : [
      'Emma Berg',
      'Ava Rice',
      'Abigail Dominica',
      'Levin McCoy',
      'Madeline Dean',
      'Natalii Rizo'
    ],
    "Historian" : [
      'Madeline Dean',
      'Natalii Rizo',
      'Abigail Dominica',
      'Ava Rice',
      'Levin McCoy',
      'Emma Berg'
    ],
    "Senator" : [
      'Emma Berg',
      'Ava Rice',
      'Abigail Dominica',
      'Levin McCoy',
      'Madeline Dean',
      'Natalii Rizo'
    ],
  },
  "2026" : {
    "President" : [
      'Emma Staubly',
      'Jacob Odongo',
      'Ansley Ziermann',
      'Alexia Quinn',
      'Genesis Tirado',
      'Gericho Sombright'
    ],
    "Vice President" : [
      'Emma Staubly',
      'Jacob Odongo',
      'Ansley Ziermann',
      'Alexia Quinn',
      'Genesis Tirado',
      'Gericho Sombright'
    ],
    "Secretary" : [
      'Emma Staubly',
      'Jacob Odongo',
      'Ansley Ziermann',
      'Alexia Quinn',
      'Genesis Tirado',
      'Gericho Sombright'
    ],
    "Treasurer" : [
      'Emma Staubly',
      'Jacob Odongo',
      'Ansley Ziermann',
      'Alexia Quinn',
      'Genesis Tirado',
      'Gericho Sombright'
    ],
    "Historian" : [
      'Alexia Quinn',
      'Emma Staubly',
      'Ansley Ziermann',
      'Genesis Tirado',
      'Jacob Odongo',
      'Gericho Sombright'
    ],
    "Senator" : [
      'Emma Staubly',
      'Jacob Odongo',
      'Ansley Ziermann',
      'Alexia Quinn',
      'Genesis Tirado',
      'Gericho Sombright'
    ],
  },
  "2027" : {
    "President" : [
      'Emma Davila',
      'Joel Barbosa',
      'Averee Cristan',
      'Anuva Ghandi',
      'Anthony Cintron',
      'Damian Brautigam'
    ],
    "Vice President" : [
      'Emma Davila',
      'Joel Barbosa',
      'Averee Cristan',
      'Anuva Ghandi',
      'Anthony Cintron',
      'Damian Brautigam'
    ],
    "Secretary" : [
      'Emma Davila',
      'Joel Barbosa',
      'Averee Cristan',
      'Anuva Ghandi',
      'Anthony Cintron',
      'Damian Brautigam'
    ],
    "Treasurer" : [
      'Emma Davila',
      'Joel Barbosa',
      'Averee Cristan',
      'Anuva Ghandi',
      'Anthony Cintron',
      'Damian Brautigam'
    ],
    "Historian" : [
      'Anuva Ghandi',
      'Emma Davila',
      'Averee Cristan',
      'Damian Brautigam',
      'Joel Barbosa',
      'Anthony Cintron'
    ],
    "Senator" : [
      'Emma Davila',
      'Joel Barbosa',
      'Averee Cristan',
      'Anuva Ghandi',
      'Anthony Cintron',
      'Damian Brautigam'
    ],
  },
}

export default choices;