import './App.css';

const Position = ({position, chosen, setChosen}) => {
  return (
    <button
      className={"position"
      + (chosen === position ? " selPosition" : "")
      }
      onClick={() => setChosen(position)}
    >
      {position}
    </button>
  )
}

export default Position