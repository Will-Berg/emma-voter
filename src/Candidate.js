import {React, useState} from 'react'
import './App.css';

const Candidate = ({name, year, activeYear, setActiveYear, chosen, setChosen}) => {
  const [selected, setSelected] = useState(false);

  return (
    <button
      className={"candidate"
      + (activeYear !== year ?
        (selected ? " inaSelCandidate" : " inaCandidate") :
        (selected ? " actSelCandidate" : "")
      )}
      onClick={() => {
        if (!selected) {
          setSelected(true);
          setChosen([...chosen, name])
        }
        else {
          setSelected(false);
          setChosen(chosen.filter((chosenName) => chosenName !== name));
        }
        setActiveYear(year);
      }}
    >
      {name}
    </button>
  )
}

export default Candidate