import './App.css';
import Year from './Year';
import Position from './Position'
import {useState} from 'react'
import choices from './Choices'

function App() {
  const [activeYear, setActiveYear] = useState("0");
  const [position, setPosition] = useState("");
  const [chosen25, setChosen25] = useState([]);
  const [chosen26, setChosen26] = useState([]);
  const [chosen27, setChosen27] = useState([]);
  const [result, setResult] = useState("The result will appear here.");
  const members25 = ['Emma Berg', 'Madeline Dean', 'Abigail Dominica', 'Levin McCoy', 'Ava Rice', 'Natalii Rizo'];
  const members26 = ['Jacob Odongo', 'Alexia Quinn', 'Gericho Sombright', 'Emma Staubly', 'Genesis Tirado', 'Ansley Ziermann'];
  const members27 = ['Joel Barbosa', 'Damian Brautigam', 'Anthony Cintron', 'Averee Cristan', 'Emma Davila', 'Anuva Ghandi'];

  const getResult = function(chosen, year) {
    let found = false;
    if (position === "") {
      setResult('No position selected.')
      return;
    }
    if (chosen.length === 0) {
      setResult('No candidates selected for this class.')
      return;
    }
    choices[year][position].forEach((option) => {
      if (chosen.includes(option) && !found) {
        setResult('Emma\'s vote for Class of ' + year + ' ' + position + ' is ' + option + '.');
        found = true;
      }
    })
  }

  return (
    <div className="App">
      <div className="top"/>
      <h1 className="title">⭐ EMMA'S BRAIN ⭐</h1>
      <h2 className="subtitle">🎩 Site made by Will Berg 🎩</h2>
      <h1 className="query">Which position is being voted on?</h1>
      <div className="row positions">
        <Position position="President" chosen={position} setChosen={setPosition}/>
        <Position position="Vice President" chosen={position} setChosen={setPosition}/>
        <Position position="Secretary" chosen={position} setChosen={setPosition}/>
        <Position position="Treasurer" chosen={position} setChosen={setPosition}/>
        <Position position="Historian" chosen={position} setChosen={setPosition}/>
        <Position position="Senator" chosen={position} setChosen={setPosition}/>
      </div>
      <h1 className="query">Which candidates are included?</h1>
      <div className="row years">
        <Year year="2025" members={members25} activeYear={activeYear} setActiveYear={setActiveYear} chosen={chosen25} setChosen={setChosen25} getResult={getResult}/>
        <div className="line"/>
        <Year year="2026" members={members26} activeYear={activeYear} setActiveYear={setActiveYear} chosen={chosen26} setChosen={setChosen26} getResult={getResult}/>
        <div className="line"/>
        <Year year="2027" members={members27} activeYear={activeYear} setActiveYear={setActiveYear} chosen={chosen27} setChosen={setChosen27} getResult={getResult}/>
      </div>
      <h2>{result}</h2>
      <div className="bottom"/>
    </div>
  );
}

export default App;
